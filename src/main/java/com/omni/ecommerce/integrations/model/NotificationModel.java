package com.omni.ecommerce.integrations.model;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
public class NotificationModel implements Serializable {
    private static final long serialVersionUID = -8721630583196433433L;
    private String to;
    private String subject;
    private String text;
}
