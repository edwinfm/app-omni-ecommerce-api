package com.omni.ecommerce.integrations.dao;

import com.omni.ecommerce.integrations.entity.ProductEntity;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ProductDao extends PagingAndSortingRepository<ProductEntity, String> {
    Page<ProductEntity> findByDeletedAtNull(Pageable pageable);

    ProductEntity findByProductsId(int id);
}
