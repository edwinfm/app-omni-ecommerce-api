package com.omni.ecommerce.integrations.dao;

import com.omni.ecommerce.integrations.entity.ShippingEntity;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;


public interface ShippingDao extends PagingAndSortingRepository<ShippingEntity, Integer> {

    ShippingEntity findByShippingId(int shippingId);

    Page<ShippingEntity> findByOrdersId(int OrdersId, Pageable pageable);
}
