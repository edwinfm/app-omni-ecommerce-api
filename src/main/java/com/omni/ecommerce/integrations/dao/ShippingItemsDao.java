package com.omni.ecommerce.integrations.dao;

import com.omni.ecommerce.integrations.entity.ShippingItemsEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ShippingItemsDao extends CrudRepository<ShippingItemsEntity, String> {
    List<ShippingItemsEntity> findById_ShippingId(int shippingId);

    ShippingItemsEntity findById_ShippingIdAndId_OrderItemsId(int shippingId, int orderItemsId);

}
