package com.omni.ecommerce.integrations.dao;

import com.omni.ecommerce.integrations.entity.OrderItemEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface OrderItemDao extends CrudRepository<OrderItemEntity, String> {
    OrderItemEntity findByOrderItemsId(int orderItemsId);
    List<OrderItemEntity> findByOrdesIdAndDeletedAtNull(int odersId);
}
