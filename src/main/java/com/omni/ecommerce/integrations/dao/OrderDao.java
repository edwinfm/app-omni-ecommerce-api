package com.omni.ecommerce.integrations.dao;

import com.omni.ecommerce.integrations.entity.OrderEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface OrderDao extends CrudRepository<OrderEntity, String> {
    OrderEntity findByOrdersId(int id);
}
