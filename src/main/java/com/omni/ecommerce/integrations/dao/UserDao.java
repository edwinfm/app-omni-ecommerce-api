package com.omni.ecommerce.integrations.dao;

import com.omni.ecommerce.integrations.entity.UserEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserDao extends CrudRepository<UserEntity, String> {
    UserEntity findByUsersId(int id);
}
