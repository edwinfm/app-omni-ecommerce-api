package com.omni.ecommerce.integrations.servicebus;

import com.omni.ecommerce.configs.RabbitConfig;
import com.omni.ecommerce.integrations.model.NotificationModel;
import lombok.RequiredArgsConstructor;
import org.springframework.amqp.AmqpException;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.retry.annotation.Backoff;
import org.springframework.retry.annotation.Retryable;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

import javax.inject.Inject;

@Component
@RequiredArgsConstructor(onConstructor = @__(@Inject))
public class QueuePublish {

    private final RabbitTemplate rabbitTemplate;

    @Async
    @Retryable(value = {AmqpException.class}, maxAttemptsExpression = "#{${server.retry.policy.max.attempts:3}}", backoff = @Backoff(delayExpression = "#{${server.retry.policy.delay:36000}}", multiplierExpression = "#{${server.retry.policy.multiplier:2}}", maxDelayExpression = "#{${server.retry.policy.max.delay:252000}}"))
    public void doSendMessageToRabbitMQ(NotificationModel notificationModel) throws AmqpException {

        String correlationId = "omni";

        rabbitTemplate.convertAndSend(RabbitConfig.QUEUE_NAME, notificationModel, m -> {
            m.getMessageProperties().setCorrelationId(correlationId);
            return m;
        });

    }

}
