package com.omni.ecommerce.integrations.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "shipping")
public class ShippingEntity implements Serializable {
    private static final long serialVersionUID = 6830231119612290663L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "shipping_id",unique=true, nullable = false)
    private int shippingId;
    private String createdAt;
    private int ordersId;
    private String trackingNumber;
    private String status;
}
