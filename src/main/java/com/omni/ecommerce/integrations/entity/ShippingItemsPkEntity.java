package com.omni.ecommerce.integrations.entity;


import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Embeddable
public class ShippingItemsPkEntity implements Serializable  {
    private static final long serialVersionUID = -801120871962944571L;

    @Column(name = "shipping_id",unique=true, nullable = false)
    private int shippingId;
    @Column(name = "order_items_id",unique=true, nullable = false)
    private int orderItemsId;




}
