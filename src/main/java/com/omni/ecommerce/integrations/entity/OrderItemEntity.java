package com.omni.ecommerce.integrations.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "order_items")
public class OrderItemEntity implements Serializable {
    private static final long serialVersionUID = -5727617139548303695L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "order_items_id",unique=true, nullable = false)
    private int orderItemsId;
    private int ordesId;

    @Column(name = "products_id",unique=true, nullable = false)
    private int productsId;
    private int quantity;
    private int price;
    private String deletedAt;

    @JoinColumn(name = "products_id", nullable = false, insertable = false,  updatable = false)
    @ManyToOne(optional = false, cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    private ProductEntity product;
}
