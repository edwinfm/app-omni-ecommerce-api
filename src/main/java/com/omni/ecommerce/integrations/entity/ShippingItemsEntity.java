package com.omni.ecommerce.integrations.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;


@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "shipping_has_order_items")
public class ShippingItemsEntity implements Serializable {

    private static final long serialVersionUID = -3042730763782917120L;
    @EmbeddedId
    @GeneratedValue(strategy = GenerationType.AUTO)
    private ShippingItemsPkEntity id;


    @JoinColumn(name = "shipping_id", nullable = false, insertable = false,  updatable = false)
    @ManyToOne(optional = false, cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    private ShippingEntity shipping;


    @JoinColumn(name = "order_items_id", nullable = false, insertable = false,  updatable = false)
    @ManyToOne(optional = false, cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    private OrderItemEntity orderItems;


}
