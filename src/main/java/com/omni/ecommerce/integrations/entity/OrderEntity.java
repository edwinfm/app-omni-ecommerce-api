package com.omni.ecommerce.integrations.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "orders")
public class OrderEntity implements Serializable {
    private static final long serialVersionUID = -3457624245083190365L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "orders_id",unique=true, nullable = false)
    private int ordersId;
    private String shippingAddress;

    @Column(name = "users_id", unique = true, nullable = false)
    private int usersId;
    private String createdAt;
    private String deletedAt;

    @JoinColumn(name = "users_id", nullable = false, insertable = false,  updatable = false)
    @ManyToOne(optional = false, cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    private UserEntity user;


}
