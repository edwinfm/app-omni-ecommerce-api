package com.omni.ecommerce.bisiness;


import com.omni.ecommerce.commons.dto.*;
import com.omni.ecommerce.commons.enums.EDateFormat;
import com.omni.ecommerce.commons.utils.DateUtil;
import com.omni.ecommerce.commons.utils.convert.GeneralConvert;
import com.omni.ecommerce.commons.utils.convert.OrderConvert;
import com.omni.ecommerce.integrations.dao.*;
import com.omni.ecommerce.integrations.entity.OrderEntity;
import com.omni.ecommerce.integrations.entity.OrderItemEntity;
import lombok.AllArgsConstructor;
import lombok.NonNull;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.support.TransactionTemplate;
import reactor.core.publisher.Mono;
import reactor.core.scheduler.Scheduler;

import javax.inject.Inject;
import java.util.List;

@Service
@AllArgsConstructor(onConstructor = @__(@Inject))
public class OrderBusiness {

    private final @NonNull OrderDao orderDao;
    private final @NonNull OrderItemDao orderItemDao;
    private final @NonNull TransactionTemplate transactionTemplate;

    @Qualifier("jdbcScheduler")
    private final @NonNull Scheduler jdbcScheduler;

    public Mono<RespOrderDto> obtainById(int id) {
        return Mono.fromCallable(() -> this.transactionTemplate.execute(
                status -> {
                    OrderEntity orderEntity = this.orderDao.findByOrdersId(id);
                    List<OrderItemEntity> orderItemEntities = this.orderItemDao
                            .findByOrdesIdAndDeletedAtNull(orderEntity.getOrdersId());
                    return OrderConvert.toDto(orderEntity, orderItemEntities);
                }
        )).subscribeOn(this.jdbcScheduler);
    }

    public Mono<RespResultDto> save(ReqOrderDto reqOrderDto) {
        return Mono.fromCallable(() -> transactionTemplate.execute(
                status -> {
                    this.orderDao.save(OrderConvert.toEntity(reqOrderDto));
                    return GeneralConvert.getRespResultDto();
                }
        )).subscribeOn(jdbcScheduler);
    }

    public Mono<RespOrderDto> update(ReqOrderUpdateDto reqOrderUpdateDto) {
        return Mono.fromCallable(() -> transactionTemplate.execute(
                status -> {
                    OrderEntity orderEntity = this.orderDao.findByOrdersId(reqOrderUpdateDto.getId());
                    orderEntity.setShippingAddress(reqOrderUpdateDto.getDireccionEnvio());
                    orderEntity.setOrdersId(reqOrderUpdateDto.getId());
                    this.orderDao.save(orderEntity);
                    List<OrderItemEntity> orderItemEntities = this.orderItemDao
                            .findByOrdesIdAndDeletedAtNull(orderEntity.getOrdersId());
                    return OrderConvert.toDto(orderEntity, orderItemEntities);
                }
        )).subscribeOn(jdbcScheduler);
    }

    public Mono<RespOrderMiniDto> delete(int id) {
        return Mono.fromCallable(() -> transactionTemplate.execute(status -> {
            OrderEntity orderEntity = this.orderDao.findByOrdersId(id);
            orderEntity.setDeletedAt(DateUtil.getDateString(EDateFormat.ISO_8601_LONG.getFormat()));
            return OrderConvert.toDto(this.orderDao.save(orderEntity));
        })).subscribeOn(jdbcScheduler);
    }

}
