package com.omni.ecommerce.bisiness;

import com.omni.ecommerce.commons.dto.ReqItemCreateDto;
import com.omni.ecommerce.commons.dto.ReqItemUpdateDto;
import com.omni.ecommerce.commons.dto.ResItemDto;
import com.omni.ecommerce.commons.dto.RespResultDto;
import com.omni.ecommerce.commons.enums.EDateFormat;
import com.omni.ecommerce.commons.utils.DateUtil;
import com.omni.ecommerce.commons.utils.convert.GeneralConvert;
import com.omni.ecommerce.commons.utils.convert.OrderItemsConvert;
import com.omni.ecommerce.integrations.dao.OrderItemDao;
import com.omni.ecommerce.integrations.entity.OrderItemEntity;
import lombok.AllArgsConstructor;
import lombok.NonNull;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.support.TransactionTemplate;
import reactor.core.publisher.Mono;
import reactor.core.scheduler.Scheduler;

import javax.inject.Inject;

@Service
@AllArgsConstructor(onConstructor = @__(@Inject))
public class ItemBusiness {

    private final @NonNull OrderItemDao orderItemDao;
    private final @NonNull TransactionTemplate transactionTemplate;

    @Qualifier("jdbcScheduler")
    private final @NonNull Scheduler jdbcScheduler;

    public Mono<ResItemDto> obtainAllByOrderId(int orderId) {
        return Mono.defer(() -> Mono
                .just(OrderItemsConvert
                        .toDto(this.orderItemDao.findByOrdesIdAndDeletedAtNull(orderId))))
                .subscribeOn(jdbcScheduler);
    }

    public Mono<ResItemDto> obtainById(int id) {
        return Mono.defer(() -> Mono
                .just(OrderItemsConvert
                        .toDto(this.orderItemDao.findByOrderItemsId(id))))
                .subscribeOn(jdbcScheduler);
    }

    public Mono<RespResultDto> save(ReqItemCreateDto reqItemCreateDto) {
        return Mono.fromCallable(() -> transactionTemplate.execute(
                status -> {
                    OrderItemEntity orderItemEntity = OrderItemsConvert.toEntity(reqItemCreateDto);
                    this.orderItemDao.save(orderItemEntity);
                    return GeneralConvert.getRespResultDto();
                }
        )).subscribeOn(jdbcScheduler);
    }

    public Mono<ResItemDto> update(ReqItemUpdateDto reqItemUpdateDto) {

        return Mono.fromCallable(() -> transactionTemplate.execute(

                status -> {
                    OrderItemEntity orderItemEntity = this.orderItemDao.findByOrderItemsId(reqItemUpdateDto.getId());
                    orderItemEntity.setQuantity(reqItemUpdateDto.getCantidad());
                    orderItemEntity.setPrice(reqItemUpdateDto.getPrecio());
                    this.orderItemDao.save(orderItemEntity);
                    return OrderItemsConvert.toDto(orderItemEntity);
                }
        )).subscribeOn(jdbcScheduler);
    }

    public Mono<ResItemDto> delete(int id) {
        return Mono.fromCallable(() -> transactionTemplate.execute(status -> {
            OrderItemEntity productEntity = this.orderItemDao.findByOrderItemsId(id);
            productEntity.setDeletedAt(DateUtil.getDateString(EDateFormat.ISO_8601_LONG.getFormat()));
            return OrderItemsConvert.toDto(this.orderItemDao.save(productEntity));
        })).subscribeOn(jdbcScheduler);
    }
}
