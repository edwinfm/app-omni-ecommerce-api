package com.omni.ecommerce.bisiness;

import com.omni.ecommerce.commons.dto.ReqShippingItemsDto;
import com.omni.ecommerce.commons.dto.RespResultDto;
import com.omni.ecommerce.commons.dto.RespShippingItemsDto;
import com.omni.ecommerce.commons.exceptions.BusinessException;
import com.omni.ecommerce.commons.utils.convert.GeneralConvert;
import com.omni.ecommerce.commons.utils.convert.ShippingItemsConvert;
import com.omni.ecommerce.integrations.dao.OrderItemDao;
import com.omni.ecommerce.integrations.dao.ShippingDao;
import com.omni.ecommerce.integrations.dao.ShippingItemsDao;
import com.omni.ecommerce.integrations.entity.OrderItemEntity;
import com.omni.ecommerce.integrations.entity.ShippingEntity;
import com.omni.ecommerce.integrations.entity.ShippingItemsEntity;
import com.omni.ecommerce.integrations.entity.ShippingItemsPkEntity;
import lombok.AllArgsConstructor;
import lombok.NonNull;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.support.TransactionTemplate;
import reactor.core.publisher.Mono;
import reactor.core.scheduler.Scheduler;

import javax.inject.Inject;
import java.util.List;

@Service
@AllArgsConstructor(onConstructor = @__(@Inject))
public class ShippingItemsBusiness {

    private final @NonNull OrderItemDao orderItemDao;
    private final @NonNull ShippingDao shippingDao;
    private final @NonNull ShippingItemsDao shippingItemsDao;
    private final @NonNull TransactionTemplate transactionTemplate;

    @Qualifier("jdbcScheduler")
    private final @NonNull Scheduler jdbcScheduler;

    public Mono<RespShippingItemsDto> obtainById(int id) {
        return Mono.fromCallable(() -> transactionTemplate.execute(
                status -> {
                    List<ShippingItemsEntity> shippingItemsEntity = this.shippingItemsDao.findById_ShippingId(id);
                    return ShippingItemsConvert.toDto(shippingItemsEntity);
                }
        )).subscribeOn(jdbcScheduler);
    }

    public Mono<RespShippingItemsDto> obtainByShippingIdAndItemId(int shippingId, int itemId) {
        return Mono.fromCallable(() -> transactionTemplate.execute(
                status -> {
                    ShippingItemsEntity shippingItemsEntity = this.shippingItemsDao.findById_ShippingIdAndId_OrderItemsId(shippingId, itemId);
                    return ShippingItemsConvert.toDto(shippingItemsEntity);
                }
        )).subscribeOn(jdbcScheduler);
    }

    public Mono<RespResultDto> save(ReqShippingItemsDto reqShippingItemsDto) {
        return Mono.fromCallable(() -> {
            OrderItemEntity orderItemEntity = this.orderItemDao.findByOrderItemsId(reqShippingItemsDto.getItemId());
            ShippingEntity shippingEntity = this.shippingDao.findByShippingId(reqShippingItemsDto.getEnvioId());
            if (orderItemEntity.getOrdesId() != shippingEntity.getOrdersId())
                throw new BusinessException("El ítem no hace parte del pedido");
            return transactionTemplate.execute(status -> {
                this.shippingItemsDao.save(ShippingItemsConvert.toEntity(reqShippingItemsDto));
                return GeneralConvert.getRespResultDto();
            });
        }).subscribeOn(jdbcScheduler);
    }

    public Mono<RespResultDto> delete(int shippingId, int itemId) {
        return Mono.fromCallable(() -> transactionTemplate.execute(
                status -> {
                    ShippingItemsEntity shippingItemsEntity = ShippingItemsConvert.toEntity(shippingId, itemId);
                    this.shippingItemsDao.delete(shippingItemsEntity);
                    return GeneralConvert.getRespResultDto();
                }
        )).subscribeOn(jdbcScheduler);
    }
}
