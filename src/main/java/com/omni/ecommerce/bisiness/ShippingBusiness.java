package com.omni.ecommerce.bisiness;


import com.omni.ecommerce.commons.dto.ReqShippingCreateDto;
import com.omni.ecommerce.commons.dto.ReqShippingUpdateDto;
import com.omni.ecommerce.commons.dto.RespShippingDto;
import com.omni.ecommerce.commons.enums.EStatusShipping;
import com.omni.ecommerce.commons.exceptions.BusinessException;
import com.omni.ecommerce.commons.utils.convert.GeneralConvert;
import com.omni.ecommerce.commons.utils.convert.ShippingConvert;
import com.omni.ecommerce.integrations.dao.OrderDao;
import com.omni.ecommerce.integrations.dao.ShippingDao;
import com.omni.ecommerce.integrations.entity.OrderEntity;
import com.omni.ecommerce.integrations.entity.ShippingEntity;
import com.omni.ecommerce.integrations.servicebus.QueuePublish;
import lombok.AllArgsConstructor;
import lombok.NonNull;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.support.TransactionTemplate;
import org.springframework.util.ObjectUtils;
import reactor.core.publisher.Mono;
import reactor.core.scheduler.Scheduler;

import javax.inject.Inject;

@Service
@AllArgsConstructor(onConstructor = @__(@Inject))
public class ShippingBusiness {

    private final @NonNull OrderDao orderDao;
    private final @NonNull ShippingDao shippingDao;
    private final @NonNull TransactionTemplate transactionTemplate;
    private final @NonNull QueuePublish queuePublish;

    @Qualifier("jdbcScheduler")
    private final @NonNull Scheduler jdbcScheduler;

    public Mono<RespShippingDto> obtainById(int id) {
        return Mono.fromCallable(() -> transactionTemplate.execute(
                status -> {
                    ShippingEntity shippingEntity = this.shippingDao.findByShippingId(id);
                    return ShippingConvert.toDto(shippingEntity);
                }
        )).subscribeOn(jdbcScheduler);
    }

    public Mono<RespShippingDto> obtainByOrderId(int orderId, int page, int size) {
        return Mono.fromCallable(() -> transactionTemplate.execute(
                status -> {
                    Pageable pageable = PageRequest.of(page, size);
                    Page<ShippingEntity> shippingEntityList = this.shippingDao.findByOrdersId(orderId, pageable);
                    return ShippingConvert.toDto(shippingEntityList);
                }
        )).subscribeOn(jdbcScheduler);
    }

    public Mono<RespShippingDto> save(ReqShippingCreateDto reqShippingCreateDto) {
        return Mono.fromCallable(() -> transactionTemplate.execute(
                status -> {
                    ShippingEntity shippingEntity = ShippingConvert.toEntity(reqShippingCreateDto);

                    return ShippingConvert.toDto(this.shippingDao.save(shippingEntity));
                }
        )).subscribeOn(jdbcScheduler);
    }

    public Mono<RespShippingDto> update(ReqShippingUpdateDto reqShippingUpdateDto) throws BusinessException {
        return Mono.fromCallable(() -> {
            EStatusShipping eStatusShipping = EStatusShipping.getByCode(reqShippingUpdateDto.getEstado());
            if (ObjectUtils.isEmpty(eStatusShipping))
                throw new BusinessException("El estado de envío no es válido");
            return transactionTemplate.execute( status -> {
                ShippingEntity shippingEntity = this.shippingDao.findByShippingId(reqShippingUpdateDto.getId());
                shippingEntity.setStatus(eStatusShipping.getCode());
                RespShippingDto respShippingDto = ShippingConvert.toDto(this.shippingDao.save(shippingEntity));
                OrderEntity orderEntity = this.orderDao.findByOrdersId(shippingEntity.getOrdersId());
                this.queuePublish.doSendMessageToRabbitMQ( GeneralConvert.getNotification(
                                orderEntity,
                                shippingEntity));
                return respShippingDto;
            });
        }).subscribeOn(jdbcScheduler);
    }

    public Mono<RespShippingDto> canceled(int id) {
        return Mono.fromCallable(() -> transactionTemplate.execute(
                status -> {
                    ShippingEntity shippingEntity = this.shippingDao.findByShippingId(id);
                    shippingEntity.setStatus(EStatusShipping.CANCELLED.getCode());
                    return ShippingConvert.toDto(this.shippingDao.save(shippingEntity));
                }
        )).subscribeOn(jdbcScheduler);
    }

}
