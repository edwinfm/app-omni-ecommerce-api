package com.omni.ecommerce.bisiness;

import com.omni.ecommerce.commons.dto.ReqProductCreateDto;
import com.omni.ecommerce.commons.dto.ReqProductUpdateDto;
import com.omni.ecommerce.commons.dto.ResProductDto;
import com.omni.ecommerce.commons.enums.EDateFormat;
import com.omni.ecommerce.commons.utils.DateUtil;
import com.omni.ecommerce.commons.utils.convert.ProductConvert;
import com.omni.ecommerce.integrations.dao.ProductDao;
import com.omni.ecommerce.integrations.entity.ProductEntity;
import lombok.AllArgsConstructor;
import lombok.NonNull;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.support.TransactionTemplate;
import reactor.core.publisher.Mono;
import reactor.core.scheduler.Scheduler;

import javax.inject.Inject;

@Service
@AllArgsConstructor(onConstructor = @__(@Inject))
public class ProductBusiness {
    private final @NonNull ProductDao productDao;
    private final @NonNull TransactionTemplate transactionTemplate;

    @Qualifier("jdbcScheduler")
    private final @NonNull Scheduler jdbcScheduler;

    public Mono<ResProductDto> obtainAll(int page, int size) {
        Pageable pageable = PageRequest.of(page, size);
        return Mono.fromCallable(() -> transactionTemplate.execute(
                status -> ProductConvert
                        .toDto(this.productDao.findByDeletedAtNull(pageable))
        )).subscribeOn(jdbcScheduler);
    }

    public Mono<ResProductDto> save(ReqProductCreateDto reqProductCreateDto) {
        return Mono.fromCallable(() -> transactionTemplate.execute(
                status -> ProductConvert
                        .toDto(this.productDao
                                .save(ProductConvert
                                        .toEntity(reqProductCreateDto)))
        )).subscribeOn(jdbcScheduler);
    }

    public Mono<ResProductDto> obtainById(int id) {

        return Mono.defer(() -> Mono
                .just(ProductConvert
                        .toDto(this.productDao.findByProductsId(id))))
                .subscribeOn(jdbcScheduler);
    }

    public Mono<ResProductDto> update(ReqProductUpdateDto reqProductUpdateDto) {
        return Mono.fromCallable(() -> transactionTemplate.execute(
                status -> ProductConvert
                        .toDto(this.productDao
                                .save(ProductConvert
                                        .toEntity(reqProductUpdateDto)))
        )).subscribeOn(jdbcScheduler);
    }

    public Mono<ResProductDto> delete(int id) {
        return Mono.fromCallable(() -> transactionTemplate.execute(status -> {
            ProductEntity productEntity = this.productDao.findByProductsId(id);
            productEntity.setDeletedAt(DateUtil.getDateString(EDateFormat.ISO_8601_LONG.getFormat()));
            return ProductConvert.toDto(this.productDao.save(productEntity));
        })).subscribeOn(jdbcScheduler);
    }

}
