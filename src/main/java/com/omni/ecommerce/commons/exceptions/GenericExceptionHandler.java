package com.omni.ecommerce.commons.exceptions;

import com.omni.ecommerce.commons.dto.RespResultDto;
import com.omni.ecommerce.commons.enums.ResponseEnum;
import org.jetbrains.annotations.NotNull;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.StringUtils;
import org.springframework.web.HttpMediaTypeNotAcceptableException;
import org.springframework.web.HttpMediaTypeNotSupportedException;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.context.request.async.AsyncRequestTimeoutException;
import org.springframework.web.servlet.NoHandlerFoundException;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.util.Collections;

@ControllerAdvice
public class GenericExceptionHandler extends ResponseEntityExceptionHandler {

    private ResponseEntity<Object> getObjectResponseEntity(Exception ex) {
        HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Type", MediaType.APPLICATION_JSON_VALUE);

        String message = ex.getLocalizedMessage();
        if (!StringUtils.hasLength(message))
            message = ResponseEnum.ERROR.getMessage();

        RespResultDto response = new RespResultDto();

        RespResultDto.Result result = new RespResultDto.Result();
        result.setCodigo(ResponseEnum.ERROR.getCode());
        result.setDescripcion(message);
        result.setTipo(ResponseEnum.ERROR.getType());
        response.setResultado(Collections.singletonList(result));

        return new ResponseEntity<>(response, headers, HttpStatus.OK);
    }

    @ExceptionHandler(Exception.class)
    public ResponseEntity<Object> handleAnyException(
            @NotNull Exception ex,
            WebRequest request) {
        return getObjectResponseEntity(ex);
    }

    @ExceptionHandler(BusinessException.class)
    public ResponseEntity<Object> handleBusinessException(
            @NotNull BusinessException ex,
            WebRequest request) {
        return getObjectResponseEntity(ex);
    }

    @NotNull
    @Override
    public ResponseEntity<Object> handleHttpMediaTypeNotSupported(
            @NotNull HttpMediaTypeNotSupportedException ex,
            @NotNull HttpHeaders headers,
            @NotNull HttpStatus status,
            @NotNull WebRequest request) {
        return getObjectResponseEntity(ex);
    }


    @NotNull
    @Override
    public ResponseEntity<Object> handleHttpMediaTypeNotAcceptable(
            @NotNull HttpMediaTypeNotAcceptableException ex,
            @NotNull HttpHeaders headers,
            @NotNull HttpStatus status,
            @NotNull WebRequest request) {
        return getObjectResponseEntity(ex);
    }

    @NotNull
    @Override
    public ResponseEntity<Object> handleHttpRequestMethodNotSupported(
            @NotNull HttpRequestMethodNotSupportedException ex,
            @NotNull HttpHeaders headers,
            @NotNull HttpStatus status,
            @NotNull WebRequest request) {
        return getObjectResponseEntity(ex);
    }

    @NotNull
    @Override
    public ResponseEntity<Object> handleNoHandlerFoundException(
            @NotNull NoHandlerFoundException ex,
            @NotNull HttpHeaders headers,
            @NotNull HttpStatus status,
            @NotNull WebRequest request) {
        return getObjectResponseEntity(ex);
    }

    @NotNull
    @Override
    public ResponseEntity<Object> handleMethodArgumentNotValid(
            @NotNull MethodArgumentNotValidException ex,
            @NotNull HttpHeaders headers,
            @NotNull HttpStatus status,
            @NotNull WebRequest request) {
        return getObjectResponseEntity(ex);
    }

    @NotNull
    @Override
    public ResponseEntity<Object> handleAsyncRequestTimeoutException(
            @NotNull AsyncRequestTimeoutException ex,
            @NotNull HttpHeaders headers,
            @NotNull HttpStatus status,
            @NotNull WebRequest webRequest) {
        return getObjectResponseEntity(ex);
    }
}
