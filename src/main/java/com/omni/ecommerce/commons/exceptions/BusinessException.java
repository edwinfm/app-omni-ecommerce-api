package com.omni.ecommerce.commons.exceptions;

import java.io.Serializable;

public class BusinessException extends Exception implements Serializable {

    private static final long serialVersionUID = 3498181008776186258L;

    public BusinessException(String message) {
        super(message);
    }

    public BusinessException(String message, Throwable cause) {
        super(message, cause);
    }

}