package com.omni.ecommerce.commons.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public enum EStatusShipping {
    RECEIVED("1", "Recibido"),
    PROCESSING("2", "Procesado"),
    ON_THE_WAY("3", "En camino"),
    DELIVERED("4", "Entregado"),
    CANCELLED("5", "Cancelado");

    private final String code;
    private final String detail;

    public static EStatusShipping getByCode(String code) {
        for (EStatusShipping e : values()) {
            if (e.code.equals(code))
                return e;
        }
        return null;
    }
}
