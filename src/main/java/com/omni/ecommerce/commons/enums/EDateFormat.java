package com.omni.ecommerce.commons.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public enum EDateFormat {
    ISO_8601_SHORT("yyyy-MM-dd"),
    ISO_8601_MEDIUM("yyyy-MM"),
    ISO_8601_MILI_LONG("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"),
    ISO_8601_LONG("yyyy-MM-dd HH:mm:ss"),
    ISO_8601_HOUR("hh:mm:ss");

    private final String format;

}
