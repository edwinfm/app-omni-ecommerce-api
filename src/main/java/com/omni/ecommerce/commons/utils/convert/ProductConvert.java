package com.omni.ecommerce.commons.utils.convert;

import com.omni.ecommerce.commons.dto.ProductDto;
import com.omni.ecommerce.commons.dto.ReqProductCreateDto;
import com.omni.ecommerce.commons.dto.ReqProductUpdateDto;
import com.omni.ecommerce.commons.dto.ResProductDto;
import com.omni.ecommerce.integrations.entity.ProductEntity;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@Component
public class ProductConvert {

    public static ResProductDto toDto(ProductEntity productEntity) {
        ResProductDto resProductDto = new ResProductDto();

        ProductDto product = new ProductDto();
        product.setId(productEntity.getProductsId());
        product.setNombre(productEntity.getName());
        product.setDescripcion(productEntity.getDescription());
        product.setPrecio(productEntity.getPrice());
        product.setEliminadoEn(productEntity.getDeletedAt());
        resProductDto.setProducto(Collections.singletonList(product));
        resProductDto.setResultado(Collections.singletonList(GeneralConvert.getResult()));
        return resProductDto;
    }

    public static ResProductDto toDto(Page<ProductEntity> productEntityList) {
        ResProductDto resProductDto = new ResProductDto();
        resProductDto.setProducto(new ArrayList<>());
        resProductDto.setPaginas(productEntityList.getTotalPages());
        for (ProductEntity productEntity : productEntityList) {
            ProductDto product = new ProductDto();
            product.setId(productEntity.getProductsId());
            product.setNombre(productEntity.getName());
            product.setDescripcion(productEntity.getDescription());
            product.setPrecio(productEntity.getPrice());
            product.setEliminadoEn(productEntity.getDeletedAt());
            resProductDto.getProducto().add(product);
        }
        resProductDto.setResultado(Collections.singletonList(GeneralConvert.getResult()));
        return resProductDto;
    }

    public static ProductEntity toEntity(ReqProductCreateDto reqProductCreateDto) {
        ProductEntity productEntity = new ProductEntity();
        productEntity.setName(reqProductCreateDto.getNombre());
        productEntity.setPrice(reqProductCreateDto.getPrecio());
        productEntity.setDescription(reqProductCreateDto.getDescripcion());
        return productEntity;
    }

    public static ProductEntity toEntity(ReqProductUpdateDto reqProductUpdateDto) {
        ProductEntity productEntity = new ProductEntity();
        productEntity.setProductsId(reqProductUpdateDto.getId());
        productEntity.setName(reqProductUpdateDto.getNombre());
        productEntity.setPrice(reqProductUpdateDto.getPrecio());
        productEntity.setDescription(reqProductUpdateDto.getDescripcion());
        return productEntity;
    }
}
