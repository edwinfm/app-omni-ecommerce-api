package com.omni.ecommerce.commons.utils.convert;

import com.omni.ecommerce.commons.dto.ProductDto;
import com.omni.ecommerce.commons.dto.ReqItemCreateDto;
import com.omni.ecommerce.commons.dto.ResItemDto;
import com.omni.ecommerce.integrations.entity.OrderItemEntity;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@Component
public class OrderItemsConvert {

    public static OrderItemEntity toEntity(ReqItemCreateDto reqItemCreateDto) {
        OrderItemEntity orderItem = new OrderItemEntity();
        orderItem.setQuantity(reqItemCreateDto.getCantidad());
        orderItem.setProductsId(reqItemCreateDto.getProducto());
        orderItem.setPrice(reqItemCreateDto.getPrecio());
        orderItem.setOrdesId(reqItemCreateDto.getPedidoId());
        return orderItem;
    }

    public static ResItemDto toDto(List<OrderItemEntity> orderItemEntityList) {
        ResItemDto resItemDto = new ResItemDto();
        resItemDto.setItems(new ArrayList<>());
        resItemDto.setResultado(Collections.singletonList(GeneralConvert.getResult()));
        for (OrderItemEntity orderItemEntity : orderItemEntityList) {
            resItemDto.getItems().add(obtainItemDao(orderItemEntity));
        }

        return resItemDto;
    }

    public static ResItemDto toDto(OrderItemEntity orderItemEntity) {
        ResItemDto resItemDto = new ResItemDto();
        resItemDto.setItems(new ArrayList<>());
        resItemDto.setResultado(Collections.singletonList(GeneralConvert.getResult()));
        resItemDto.getItems().add(obtainItemDao(orderItemEntity));

        return resItemDto;
    }

    private static ResItemDto.ItemDto obtainItemDao(OrderItemEntity orderItemEntity) {
        ResItemDto.ItemDto itemDto = new ResItemDto.ItemDto();
        itemDto.setCantidad(orderItemEntity.getQuantity());
        itemDto.setEliminadoEn(orderItemEntity.getDeletedAt());
        itemDto.setId(orderItemEntity.getOrderItemsId());
        itemDto.setProducto(new ProductDto(
                orderItemEntity.getProductsId(),
                orderItemEntity.getProduct().getName(),
                orderItemEntity.getProduct().getPrice(),
                orderItemEntity.getProduct().getDescription(),
                orderItemEntity.getProduct().getDeletedAt()));
        itemDto.setPrecio(orderItemEntity.getPrice());
        return itemDto;
    }

}
