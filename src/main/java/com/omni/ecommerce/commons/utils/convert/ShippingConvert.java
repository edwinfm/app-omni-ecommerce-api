package com.omni.ecommerce.commons.utils.convert;

import com.omni.ecommerce.commons.dto.ReqShippingCreateDto;
import com.omni.ecommerce.commons.dto.RespShippingDto;
import com.omni.ecommerce.commons.enums.EDateFormat;
import com.omni.ecommerce.commons.enums.EStatusShipping;
import com.omni.ecommerce.commons.utils.DateUtil;
import com.omni.ecommerce.integrations.entity.ShippingEntity;
import org.jetbrains.annotations.NotNull;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Collections;
import java.util.UUID;

@Component
public class ShippingConvert {

    public static RespShippingDto toDto(ShippingEntity shippingEntity) {
        RespShippingDto respShippingDto = new RespShippingDto();
        respShippingDto.setResultado(Collections.singletonList(GeneralConvert.getResult()));
        respShippingDto.setEnvios(new ArrayList<>());

        RespShippingDto.Shipping shipping = getShipping(shippingEntity);
        respShippingDto.getEnvios().add(shipping);
        return respShippingDto;
    }

    @NotNull
    private static RespShippingDto.Shipping getShipping(ShippingEntity shippingEntity) {
        RespShippingDto.Shipping shipping = new RespShippingDto.Shipping();
        shipping.setCreadoEn(shippingEntity.getCreatedAt());
        shipping.setEstado(shippingEntity.getStatus());
        shipping.setNúmeroSeguimiento(shippingEntity.getTrackingNumber());
        shipping.setPedidoId(shippingEntity.getOrdersId());
        shipping.setId(shippingEntity.getShippingId());
        return shipping;
    }

    public static RespShippingDto toDto(Page<ShippingEntity> shippingEntityList) {
        RespShippingDto respShippingDto = new RespShippingDto();
        respShippingDto.setResultado(Collections.singletonList(GeneralConvert.getResult()));
        respShippingDto.setEnvios(new ArrayList<>());
        respShippingDto.setPaginas(shippingEntityList.getTotalPages());
        for (ShippingEntity shippingEntity : shippingEntityList) {
            RespShippingDto.Shipping shipping = getShipping(shippingEntity);
            respShippingDto.getEnvios().add(shipping);
        }
        return respShippingDto;
    }

    public static ShippingEntity toEntity(ReqShippingCreateDto reqShippingCreateDto) {
        ShippingEntity shippingEntity = new ShippingEntity();
        shippingEntity.setStatus(EStatusShipping.RECEIVED.getCode());
        shippingEntity.setCreatedAt(DateUtil.getDateString(EDateFormat.ISO_8601_LONG.getFormat()));
        shippingEntity.setTrackingNumber(UUID.randomUUID().toString());
        shippingEntity.setOrdersId(reqShippingCreateDto.getPedidoId());
        return shippingEntity;
    }
}
