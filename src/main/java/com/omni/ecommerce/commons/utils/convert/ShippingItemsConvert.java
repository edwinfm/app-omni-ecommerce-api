package com.omni.ecommerce.commons.utils.convert;

import com.omni.ecommerce.commons.dto.*;
import com.omni.ecommerce.integrations.entity.OrderItemEntity;
import com.omni.ecommerce.integrations.entity.ShippingItemsEntity;
import com.omni.ecommerce.integrations.entity.ShippingItemsPkEntity;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@Component
public class ShippingItemsConvert {

    public static RespShippingItemsDto toDto(ShippingItemsEntity shippingItemsEntity) {
        RespShippingItemsDto shippingItemsDto = new RespShippingItemsDto();
        shippingItemsDto.setResultado(Collections.singletonList(GeneralConvert.getResult()));
        ShippingDto shippingDto = new ShippingDto();
        shippingDto.setItems(new ArrayList<>());
        shippingDto.getItems().add(obtainItemDao(shippingItemsEntity.getOrderItems()));
        shippingDto.setEstado(shippingItemsEntity.getShipping().getStatus());
        shippingDto.setCreadoEn(shippingItemsEntity.getShipping().getCreatedAt());
        shippingDto.setNúmeroSeguimiento(shippingItemsEntity.getShipping().getTrackingNumber());
        shippingDto.setPedidoId(shippingItemsEntity.getShipping().getOrdersId());
        shippingDto.setId(shippingItemsEntity.getShipping().getShippingId());
        shippingItemsDto.setEnvios(new ArrayList<>());
        shippingItemsDto.getEnvios().add(shippingDto);
        return shippingItemsDto;
    }

    public static RespShippingItemsDto toDto(List<ShippingItemsEntity> shippingItemsEntityList) {
        RespShippingItemsDto shippingItemsDto = new RespShippingItemsDto();
        shippingItemsDto.setResultado(Collections.singletonList(GeneralConvert.getResult()));
        ShippingDto shippingDto = new ShippingDto();
        shippingDto.setItems(new ArrayList<>());
        for ( ShippingItemsEntity shippingItemsEntity : shippingItemsEntityList) {
            shippingDto.getItems().add(obtainItemDao(shippingItemsEntity.getOrderItems()));
        }

        shippingDto.setEstado(shippingItemsEntityList.get(0).getShipping().getStatus());
        shippingDto.setCreadoEn(shippingItemsEntityList.get(0).getShipping().getCreatedAt());
        shippingDto.setNúmeroSeguimiento(shippingItemsEntityList.get(0).getShipping().getTrackingNumber());
        shippingDto.setPedidoId(shippingItemsEntityList.get(0).getShipping().getOrdersId());
        shippingDto.setId(shippingItemsEntityList.get(0).getShipping().getShippingId());
        shippingItemsDto.setEnvios(new ArrayList<>());
        shippingItemsDto.getEnvios().add(shippingDto);
        return shippingItemsDto;
    }

    public static ShippingItemsEntity toEntity(ReqShippingItemsDto reqShippingItemsDto) {
        return toEntity( reqShippingItemsDto.getEnvioId(), reqShippingItemsDto.getItemId());
    }

    public static ShippingItemsEntity toEntity(int shippingId, int itemId) {
        ShippingItemsEntity shippingItemsEntity = new ShippingItemsEntity();
        ShippingItemsPkEntity shippingItemsPkEntity = new ShippingItemsPkEntity();
        shippingItemsPkEntity.setShippingId(shippingId);
        shippingItemsPkEntity.setOrderItemsId(itemId);
        shippingItemsEntity.setId(shippingItemsPkEntity);
        return shippingItemsEntity;
    }

    private static ItemDto obtainItemDao(OrderItemEntity orderItemEntity) {
        ItemDto itemDto = new ItemDto();
        itemDto.setCantidad(orderItemEntity.getQuantity());
        itemDto.setEliminadoEn(orderItemEntity.getDeletedAt());
        itemDto.setId(orderItemEntity.getOrderItemsId());
        itemDto.setProducto(new ProductDto(
                orderItemEntity.getProductsId(),
                orderItemEntity.getProduct().getName(),
                orderItemEntity.getProduct().getPrice(),
                orderItemEntity.getProduct().getDescription(),
                orderItemEntity.getProduct().getDeletedAt()));
        itemDto.setPrecio(orderItemEntity.getPrice());
        return itemDto;
    }
}
