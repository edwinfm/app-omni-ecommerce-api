package com.omni.ecommerce.commons.utils.convert;

import com.omni.ecommerce.commons.dto.RespResultDto;
import com.omni.ecommerce.commons.enums.EStatusShipping;
import com.omni.ecommerce.commons.enums.ResponseEnum;
import com.omni.ecommerce.integrations.entity.OrderEntity;
import com.omni.ecommerce.integrations.entity.ShippingEntity;
import com.omni.ecommerce.integrations.model.NotificationModel;
import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Component;

import java.util.Collections;

@Component
public class GeneralConvert {

    private static final String SUBJECT = "Su pedido se encuentra :status";
    private static final String TEXT = "El pedido con número de seguimiento :trackingNumber y dirección de envío :address se encuentra :status.";

    @NotNull
    public static RespResultDto.Result getResult() {
        RespResultDto.Result result = new RespResultDto.Result();
        result.setCodigo(ResponseEnum.SUCCESS.getCode());
        result.setDescripcion(ResponseEnum.SUCCESS.getMessage());
        result.setTipo(ResponseEnum.SUCCESS.getType());
        return result;
    }

    @NotNull
    public static RespResultDto getRespResultDto() {
        RespResultDto respResultDto = new RespResultDto();
        respResultDto.setResultado(Collections.singletonList(getResult()));
        RespResultDto.Result result = new RespResultDto.Result();
        result.setCodigo(ResponseEnum.SUCCESS.getCode());
        result.setDescripcion(ResponseEnum.SUCCESS.getMessage());
        result.setTipo(ResponseEnum.SUCCESS.getType());
        return respResultDto;
    }

    @NotNull
    public static NotificationModel getNotification( OrderEntity orderEntity, ShippingEntity shippingEntity ) {
        String detailStatus = EStatusShipping.getByCode(shippingEntity.getStatus()).getDetail();

        NotificationModel notificationModel = new NotificationModel();
        notificationModel.setSubject( SUBJECT.replace(":status", detailStatus) );
        notificationModel.setText( TEXT
                .replace(":trackingNumber", shippingEntity.getTrackingNumber())
                .replace(":status", detailStatus)
                .replace(":address", orderEntity.getUser().getAddress()));
        notificationModel.setTo( orderEntity.getUser().getEmail() );
        return notificationModel;
    }

}
