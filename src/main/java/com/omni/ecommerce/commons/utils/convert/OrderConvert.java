package com.omni.ecommerce.commons.utils.convert;

import com.omni.ecommerce.commons.dto.*;
import com.omni.ecommerce.commons.enums.EDateFormat;
import com.omni.ecommerce.commons.utils.DateUtil;
import com.omni.ecommerce.integrations.entity.OrderEntity;
import com.omni.ecommerce.integrations.entity.OrderItemEntity;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@Component
public class OrderConvert {
    /**
     * @param orderEntity
     * @param orderItemEntityList
     * @return resultOrderDto
     */
    public static RespOrderDto toDto(OrderEntity orderEntity, List<OrderItemEntity> orderItemEntityList) {
        RespOrderDto resultOrderDto = new RespOrderDto();
        resultOrderDto.setPedido(new ArrayList<>());
        resultOrderDto.setResultado(Collections.singletonList(GeneralConvert.getResult()));

        /**
         * Se convierte de OrderJoinUserModel a Pedido y Usuario para retornar respuesta en formato RespOrderDto
         */
        RespOrderDto.Order order = new RespOrderDto.Order();
        order.setId(orderEntity.getOrdersId());
        order.setDireccionEnvio(orderEntity.getShippingAddress());
        order.setEliminadoEn(orderEntity.getDeletedAt());
        order.setCreadoEn(orderEntity.getCreatedAt());
        order.setUsuario(
                new UserDto(
                        orderEntity.getUsersId(),
                        orderEntity.getUser().getFullName(),
                        orderEntity.getUser().getAddress(),
                        orderEntity.getUser().getEmail()));

        /**
         * Se recorre List<OrderItemJoinProductModel> para agregar y convertir los Articulos y Productos
         * para retornar respuesta en formato RespOrderDto
         */
        order.setArticulos(new ArrayList<>());
        for (OrderItemEntity itemEntity : orderItemEntityList) {
            order.getArticulos().add(obtainItemDao(itemEntity));
        }
        resultOrderDto.getPedido().add(order);
        return resultOrderDto;
    }

    public static RespOrderMiniDto toDto(OrderEntity orderEntity) {
        RespOrderMiniDto respOrderDto = new RespOrderMiniDto();
        respOrderDto.setPedido(new ArrayList<>());
        RespOrderMiniDto.Order order = new RespOrderMiniDto.Order();

        order.setId(orderEntity.getOrdersId());
        order.setDireccionEnvio(orderEntity.getShippingAddress());
        order.setCreadoEn(orderEntity.getCreatedAt());
        order.setEliminadoEn(orderEntity.getDeletedAt());
        order.setUsuario( new UserDto(
                orderEntity.getUsersId(),
                orderEntity.getUser().getFullName(),
                orderEntity.getUser().getAddress(),
                orderEntity.getUser().getEmail()
        ));
        respOrderDto.getPedido().add(order);
        respOrderDto.setResultado(Collections.singletonList(GeneralConvert.getResult()));
        return respOrderDto;
    }

    public static OrderEntity toEntity(ReqOrderDto reqOrderDto) {
        OrderEntity orderEntity = new OrderEntity();
        orderEntity.setCreatedAt(DateUtil.getDateString(EDateFormat.ISO_8601_LONG.getFormat()));
        orderEntity.setShippingAddress(reqOrderDto.getDireccionEnvio());
        orderEntity.setUsersId(reqOrderDto.getUsuario());
        return orderEntity;
    }

    private static ItemDto obtainItemDao(OrderItemEntity orderItemEntity) {
        ItemDto itemDto = new ItemDto();
        itemDto.setCantidad(orderItemEntity.getQuantity());
        itemDto.setEliminadoEn(orderItemEntity.getDeletedAt());
        itemDto.setId(orderItemEntity.getOrderItemsId());
        itemDto.setProducto(
                new ProductDto(
                        orderItemEntity.getProductsId(),
                        orderItemEntity.getProduct().getName(),
                        orderItemEntity.getProduct().getPrice(),
                        orderItemEntity.getProduct().getDescription(),
                        ""));
        itemDto.setPrecio(orderItemEntity.getPrice());
        return itemDto;
    }
}
