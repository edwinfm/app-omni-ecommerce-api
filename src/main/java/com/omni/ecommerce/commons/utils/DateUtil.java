package com.omni.ecommerce.commons.utils;

import org.apache.commons.validator.routines.DateValidator;
import org.springframework.stereotype.Component;

import java.text.SimpleDateFormat;
import java.util.Date;

@Component
public class DateUtil extends DateValidator {

    public static String getDateString(String formatReturn) {
        SimpleDateFormat sdf = new SimpleDateFormat(formatReturn);
        return sdf.format(new Date());
    }
}
