package com.omni.ecommerce.commons.dto;

import lombok.*;

import java.io.Serializable;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class RespResultDto implements Serializable {
    private static final long serialVersionUID = -4765810818384797495L;
    private List<Result> resultado;

    @Data
    public static class Result {
        private String codigo;
        private String descripcion;
        private String tipo;
    }
}
