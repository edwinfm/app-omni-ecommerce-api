package com.omni.ecommerce.commons.dto;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
public class ReqOrderUpdateDto implements Serializable {
    private static final long serialVersionUID = -5631931315647883007L;
    private int id;
    private String direccionEnvio;
}
