package com.omni.ecommerce.commons.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@Setter
@Getter
@AllArgsConstructor
public class ReqProductUpdateDto implements Serializable {
    private static final long serialVersionUID = 6028912543387721573L;
    private int id;
    private String nombre;
    private int precio;
    private String descripcion;
}
