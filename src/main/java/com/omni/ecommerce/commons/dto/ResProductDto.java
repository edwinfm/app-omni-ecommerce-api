package com.omni.ecommerce.commons.dto;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.List;

@Getter
@Setter
public class ResProductDto extends RespResultDto implements Serializable {

    private static final long serialVersionUID = 8848776664929705494L;
    private List<ProductDto> producto;
    private int paginas;

}
