package com.omni.ecommerce.commons.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class ReqShippingCreateDto implements Serializable {
    private static final long serialVersionUID = -5552123532636163509L;
    private int pedidoId;
}
