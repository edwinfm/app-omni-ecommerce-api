package com.omni.ecommerce.commons.dto;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.List;

@Getter
@Setter
public class RespShippingItemsDto extends RespResultDto implements Serializable {
    private static final long serialVersionUID = -6835933200028369947L;
    private List<ShippingDto> envios;
}
