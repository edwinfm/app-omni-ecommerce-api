package com.omni.ecommerce.commons.dto;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
public class ReqOrderDto implements Serializable {
    private static final long serialVersionUID = 8147554922006471375L;
    private String direccionEnvio;
    private int usuario;
}
