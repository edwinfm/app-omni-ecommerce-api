package com.omni.ecommerce.commons.dto;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class ShippingDto {
    private int id;
    private String creadoEn;
    private int pedidoId;
    private String númeroSeguimiento;
    private String estado;
    private List<ItemDto> items;
}