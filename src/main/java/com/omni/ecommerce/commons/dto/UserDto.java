package com.omni.ecommerce.commons.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
@AllArgsConstructor
public class UserDto implements Serializable {

    private static final long serialVersionUID = 8509826026559803870L;
    private int id;
    private String nombreCompleto;
    private String direccion;
    private String email;
}
