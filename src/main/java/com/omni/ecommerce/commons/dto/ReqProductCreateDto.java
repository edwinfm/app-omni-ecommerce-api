package com.omni.ecommerce.commons.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
@AllArgsConstructor
public class ReqProductCreateDto implements Serializable {
    private static final long serialVersionUID = -5846979164545997996L;
    private String nombre;
    private int precio;
    private String descripcion;
}
