package com.omni.ecommerce.commons.dto;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
public class ReqItemUpdateDto implements Serializable {
    private static final long serialVersionUID = -1403396583626696500L;
    private int id;
    private int cantidad;
    private int precio;
}
