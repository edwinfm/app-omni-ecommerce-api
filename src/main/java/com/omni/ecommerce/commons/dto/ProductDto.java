package com.omni.ecommerce.commons.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class ProductDto implements Serializable {
    private int id;
    private String nombre;
    private int precio;
    private String descripcion;
    private String eliminadoEn;
}
