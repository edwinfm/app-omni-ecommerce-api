package com.omni.ecommerce.commons.dto;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.List;

@Getter
@Setter
public class RespShippingDto extends RespResultDto implements Serializable {
    private static final long serialVersionUID = -4897637176594545177L;
    private List<Shipping> envios;
    private int paginas;

    @Getter
    @Setter
    public static class Shipping {
        private int id;
        private String creadoEn;
        private int pedidoId;
        private String númeroSeguimiento;
        private String estado;
    }
}
