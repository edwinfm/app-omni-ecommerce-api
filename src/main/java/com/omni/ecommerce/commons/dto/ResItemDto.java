package com.omni.ecommerce.commons.dto;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.List;

@Getter
@Setter
public class ResItemDto extends RespResultDto implements Serializable {
    private static final long serialVersionUID = -3395954638888700162L;
    private List<ItemDto> items;

    @Getter
    @Setter
    public static class ItemDto {
        private int id;
        private ProductDto producto;
        private int cantidad;
        private int precio;
        private String eliminadoEn;
    }
}
