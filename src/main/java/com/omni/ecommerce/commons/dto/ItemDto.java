package com.omni.ecommerce.commons.dto;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
public class ItemDto implements Serializable {
    private static final long serialVersionUID = 7002107226645626967L;
    private int id;
    private ProductDto producto;
    private int cantidad;
    private int precio;
    private String eliminadoEn;
}
