package com.omni.ecommerce.commons.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class ReqShippingItemsDto implements Serializable {
    private static final long serialVersionUID = 2948311310315014739L;

    private int envioId;
    private int itemId;
}
