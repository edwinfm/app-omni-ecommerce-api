package com.omni.ecommerce.commons.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
@AllArgsConstructor
public class ReqShippingUpdateDto implements Serializable {
    private static final long serialVersionUID = 4660819036685897420L;
    private int id;
    private String estado;
}
