package com.omni.ecommerce.commons.dto;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
public class ReqItemCreateDto implements Serializable {
    private static final long serialVersionUID = 6503156867880666428L;
    private int producto;
    private int pedidoId;
    private int cantidad;
    private int precio;
}
