package com.omni.ecommerce.commons.dto;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.List;

@Getter
@Setter
public class RespOrderDto extends RespResultDto implements Serializable {

    private static final long serialVersionUID = -133979475035254193L;
    private List<Order> pedido;

    @Getter
    @Setter
    public static class Order {
        private int id;
        private String direccionEnvio;
        private UserDto usuario;
        private String eliminadoEn;
        private String creadoEn;
        private List<ItemDto> articulos;
    }
}
