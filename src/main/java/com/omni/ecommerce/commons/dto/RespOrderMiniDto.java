package com.omni.ecommerce.commons.dto;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.List;

@Getter
@Setter
public class RespOrderMiniDto extends RespResultDto implements Serializable {
    private List<Order> pedido;

    @Getter
    @Setter
    public static class Order {
        private int id;
        private String direccionEnvio;
        private UserDto usuario;
        private String eliminadoEn;
        private String creadoEn;
    }
}
