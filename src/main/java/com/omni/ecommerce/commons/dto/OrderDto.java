package com.omni.ecommerce.commons.dto;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.List;

@Getter
@Setter
public class OrderDto implements Serializable {

    private static final long serialVersionUID = -9134318398633806432L;
    private int id;
    private String direccionEnvio;
    private int usuario;
    private String eliminadoEn;
    private String creadoEn;
    private List<ItemDto> articulos;

}
