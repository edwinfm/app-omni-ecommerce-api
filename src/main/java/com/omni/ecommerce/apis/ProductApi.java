package com.omni.ecommerce.apis;

import com.omni.ecommerce.bisiness.ProductBusiness;
import com.omni.ecommerce.commons.dto.ReqProductCreateDto;
import com.omni.ecommerce.commons.dto.ReqProductUpdateDto;
import com.omni.ecommerce.commons.dto.ResProductDto;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import lombok.NonNull;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Mono;

import javax.inject.Inject;

@RestController
@RequestMapping("productos")
@AllArgsConstructor(onConstructor = @__(@Inject))
public class ProductApi {

    private final @NonNull ProductBusiness productBusiness;

    @ApiOperation(value = "Listar producto", produces = MediaType.APPLICATION_JSON_VALUE, response = ResProductDto.class)
    @GetMapping(value = "", produces = MediaType.APPLICATION_JSON_VALUE)
    public Mono<ResProductDto> obtainAll(
            @RequestParam( name = "pagina") int page,
            @RequestParam( name = "tamaño") int size
    ) {
        return this.productBusiness.obtainAll(page, size);
    }

    @ApiOperation(value = "Obtener producto por id", produces = MediaType.APPLICATION_JSON_VALUE, response = ResProductDto.class)
    @GetMapping(value = "{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public Mono<ResProductDto> obtainById(@PathVariable(name = "id") int id) {
        return this.productBusiness.obtainById(id);
    }

    @ApiOperation(value = "Crear nuevo producto", produces = MediaType.APPLICATION_JSON_VALUE, response = ResProductDto.class)
    @PostMapping(value = "", produces = MediaType.APPLICATION_JSON_VALUE)
    public Mono<ResProductDto> save(@RequestBody ReqProductCreateDto reqProductCreateDto) {
        return this.productBusiness.save(reqProductCreateDto);
    }

    @ApiOperation(value = "Actualizar producto", produces = MediaType.APPLICATION_JSON_VALUE, response = ResProductDto.class)
    @PutMapping(value = "", produces = MediaType.APPLICATION_JSON_VALUE)
    public Mono<ResProductDto> update(@RequestBody ReqProductUpdateDto reqProductUpdateDto) {
        return this.productBusiness.update(reqProductUpdateDto);
    }

    @ApiOperation(value = "Eliminar producto", produces = MediaType.APPLICATION_JSON_VALUE, response = ResProductDto.class)
    @DeleteMapping(value = "{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public Mono<ResProductDto> delete(@PathVariable(name = "id") int id) {
        return this.productBusiness.delete(id);
    }
}
