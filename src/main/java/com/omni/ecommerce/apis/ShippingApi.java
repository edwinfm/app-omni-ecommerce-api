package com.omni.ecommerce.apis;

import com.omni.ecommerce.bisiness.ShippingBusiness;
import com.omni.ecommerce.commons.dto.*;
import com.omni.ecommerce.commons.exceptions.BusinessException;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import lombok.NonNull;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Mono;

import javax.inject.Inject;

@RestController
@RequestMapping("envios")
@AllArgsConstructor(onConstructor = @__(@Inject))
public class ShippingApi {

    private final @NonNull ShippingBusiness shippingBusiness;

    @ApiOperation(value = "Listar envios por id de pedido", produces = MediaType.APPLICATION_JSON_VALUE, response = RespShippingDto.class)
    @GetMapping(value = "", produces = MediaType.APPLICATION_JSON_VALUE)
    public Mono<RespShippingDto> obtainAll(
            @RequestParam( name = "pedidoId") int orderId,
            @RequestParam( name = "pagina") int page,
            @RequestParam( name = "tamaño") int size) {
        return this.shippingBusiness.obtainByOrderId(orderId, page, size);
    }

    @ApiOperation(value = "Obtener envio por id", produces = MediaType.APPLICATION_JSON_VALUE, response = RespShippingDto.class)
    @GetMapping(value = "{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public Mono<RespShippingDto> obtainById(@PathVariable(name = "id") int id) {
        return this.shippingBusiness.obtainById(id);
    }

    @ApiOperation(value = "Crear envio", produces = MediaType.APPLICATION_JSON_VALUE, response = RespShippingDto.class)
    @PostMapping(value = "", produces = MediaType.APPLICATION_JSON_VALUE)
    public Mono<RespShippingDto> save(@RequestBody ReqShippingCreateDto reqShippingCreateDto) {
        return this.shippingBusiness.save(reqShippingCreateDto);
    }

    @ApiOperation(value = "Actualizar estado de envio", produces = MediaType.APPLICATION_JSON_VALUE, response = RespShippingDto.class)
    @PutMapping(value = "", produces = MediaType.APPLICATION_JSON_VALUE)
    public Mono<RespShippingDto> update(@RequestBody ReqShippingUpdateDto reqShippingUpdateDto) throws BusinessException {
        return this.shippingBusiness.update(reqShippingUpdateDto);
    }

    @ApiOperation(value = "Cancelar envio", produces = MediaType.APPLICATION_JSON_VALUE, response = RespShippingDto.class)
    @DeleteMapping(value = "{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public Mono<RespShippingDto> delete(@PathVariable(name = "id") int id) {
        return this.shippingBusiness.canceled(id);
    }
}
