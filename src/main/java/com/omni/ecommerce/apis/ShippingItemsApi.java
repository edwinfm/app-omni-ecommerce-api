package com.omni.ecommerce.apis;

import com.omni.ecommerce.bisiness.ShippingItemsBusiness;
import com.omni.ecommerce.commons.dto.ReqShippingItemsDto;
import com.omni.ecommerce.commons.dto.RespResultDto;
import com.omni.ecommerce.commons.dto.RespShippingItemsDto;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import lombok.NonNull;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Mono;

import javax.inject.Inject;

@RestController
@RequestMapping("envios/items")
@AllArgsConstructor(onConstructor = @__(@Inject))
public class ShippingItemsApi {

    private final @NonNull ShippingItemsBusiness shippingItemsBusiness;

    @ApiOperation(value = "Obtener items de envio", produces = MediaType.APPLICATION_JSON_VALUE, response = RespShippingItemsDto.class)
    @GetMapping(value = "{envioId}/{itemId}", produces = MediaType.APPLICATION_JSON_VALUE)
    public Mono<RespShippingItemsDto> obtainByShippingIdAndItemId(
            @PathVariable(name = "envioId") int shippingId,
            @PathVariable(name = "itemId") int itemId) {
        return this.shippingItemsBusiness.obtainByShippingIdAndItemId(shippingId, itemId);
    }

    @ApiOperation(value = "Listar items por id de envio", produces = MediaType.APPLICATION_JSON_VALUE, response = RespShippingItemsDto.class)
    @GetMapping(value = "", produces = MediaType.APPLICATION_JSON_VALUE)
    public Mono<RespShippingItemsDto> obtainAll(@RequestParam(name = "envioId") int shippingId) {
        return this.shippingItemsBusiness.obtainById(shippingId);
    }

    @ApiOperation(value = "Crear nuevo items de envio", produces = MediaType.APPLICATION_JSON_VALUE, response = RespResultDto.class)
    @PostMapping(value = "", produces = MediaType.APPLICATION_JSON_VALUE)
    public Mono<RespResultDto> save(@RequestBody ReqShippingItemsDto reqShippingItemsDto) {
        return this.shippingItemsBusiness.save(reqShippingItemsDto);
    }

    @ApiOperation(value = "Eliminar items de envio", produces = MediaType.APPLICATION_JSON_VALUE, response = RespResultDto.class)
    @DeleteMapping(value = "{envioId}/{itemId}", produces = MediaType.APPLICATION_JSON_VALUE)
    public Mono<RespResultDto> delete(
            @PathVariable(name = "envioId") int shippingId,
            @PathVariable(name = "itemId") int itemId) {
        return this.shippingItemsBusiness.delete(shippingId, itemId);
    }
}
