package com.omni.ecommerce.apis;

import com.omni.ecommerce.bisiness.ItemBusiness;
import com.omni.ecommerce.commons.dto.ReqItemCreateDto;
import com.omni.ecommerce.commons.dto.ReqItemUpdateDto;
import com.omni.ecommerce.commons.dto.ResItemDto;
import com.omni.ecommerce.commons.dto.RespResultDto;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import lombok.NonNull;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Mono;

import javax.inject.Inject;

@RestController
@RequestMapping("pedidos/items")
@AllArgsConstructor(onConstructor = @__(@Inject))
public class ItemApi {

    private final @NonNull ItemBusiness itemBusiness;

    @ApiOperation(value = "Listar items por id de pedido", produces = MediaType.APPLICATION_JSON_VALUE, response = ResItemDto.class)
    @GetMapping(value = "", produces = MediaType.APPLICATION_JSON_VALUE)
    public Mono<ResItemDto> obtainAll( @RequestParam( name = "pedidoId") int orderId) {
        return this.itemBusiness.obtainAllByOrderId(orderId);
    }

    @ApiOperation(value = "Obtener items de pedido por id", produces = MediaType.APPLICATION_JSON_VALUE, response = ResItemDto.class)
    @GetMapping(value = "{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public Mono<ResItemDto> obtainById(@PathVariable(name = "id") int id) {
        return this.itemBusiness.obtainById(id);
    }

    @ApiOperation(value = "Crear nuevo items de pedido", produces = MediaType.APPLICATION_JSON_VALUE, response = RespResultDto.class)
    @PostMapping(value = "", produces = MediaType.APPLICATION_JSON_VALUE)
    public Mono<RespResultDto> save(@RequestBody ReqItemCreateDto reqItemCreateDto) {
        return this.itemBusiness.save(reqItemCreateDto);
    }

    @ApiOperation(value = "Actualizar items de pedido", produces = MediaType.APPLICATION_JSON_VALUE, response = ResItemDto.class)
    @PutMapping(value = "", produces = MediaType.APPLICATION_JSON_VALUE)
    public Mono<ResItemDto> update(@RequestBody ReqItemUpdateDto reqItemUpdateDto) {
        return this.itemBusiness.update(reqItemUpdateDto);
    }

    @ApiOperation(value = "Eliminar items de pedido", produces = MediaType.APPLICATION_JSON_VALUE, response = ResItemDto.class)
    @DeleteMapping(value = "{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public Mono<ResItemDto> delete(@PathVariable(name = "id") int id) {
        return this.itemBusiness.delete(id);
    }
}
