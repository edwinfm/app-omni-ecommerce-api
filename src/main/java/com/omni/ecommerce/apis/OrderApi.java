package com.omni.ecommerce.apis;

import com.omni.ecommerce.bisiness.OrderBusiness;
import com.omni.ecommerce.commons.dto.*;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import lombok.NonNull;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Mono;

import javax.inject.Inject;

@RestController
@RequestMapping("pedidos")
@AllArgsConstructor(onConstructor = @__(@Inject))
public class OrderApi {

    private final @NonNull OrderBusiness orderBusiness;

    @ApiOperation(value = "Obtener pedido por id", produces = MediaType.APPLICATION_JSON_VALUE, response = RespOrderDto.class)
    @GetMapping(value = "{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public Mono<RespOrderDto> obtainById(@PathVariable(name = "id") int id) {
        return this.orderBusiness.obtainById(id);
    }

    @ApiOperation(value = "Crear nuevo pedido", produces = MediaType.APPLICATION_JSON_VALUE, response = RespResultDto.class)
    @PostMapping(value = "", produces = MediaType.APPLICATION_JSON_VALUE)
    public Mono<RespResultDto> save(@RequestBody ReqOrderDto reqOrderDto) {
        return this.orderBusiness.save(reqOrderDto);
    }

    @ApiOperation(value = "Actualizar pedido", produces = MediaType.APPLICATION_JSON_VALUE, response = RespOrderDto.class)
    @PutMapping(value = "", produces = MediaType.APPLICATION_JSON_VALUE)
    public Mono<RespOrderDto> update(@RequestBody ReqOrderUpdateDto reqOrderUpdateDto) {
        return this.orderBusiness.update(reqOrderUpdateDto);
    }

    @ApiOperation(value = "Eliminar pedido", produces = MediaType.APPLICATION_JSON_VALUE, response = RespOrderDto.class)
    @DeleteMapping(value = "{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public Mono<RespOrderMiniDto> delete(@PathVariable(name = "id") int id) {
        return this.orderBusiness.delete(id);
    }
}
