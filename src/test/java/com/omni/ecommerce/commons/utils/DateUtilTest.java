package com.omni.ecommerce.commons.utils;

import com.omni.ecommerce.commons.enums.EDateFormat;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

class DateUtilTest {

    DateUtil dateUtil = new DateUtil();

    @Test
    void getDateString() {
        String date = dateUtil.getDateString(EDateFormat.ISO_8601_MILI_LONG.getFormat());
        Assertions.assertNotEquals("", date);
    }
}