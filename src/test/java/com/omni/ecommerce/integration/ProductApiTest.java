package com.omni.ecommerce.integration;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.omni.ecommerce.commons.dto.ReqProductCreateDto;
import com.omni.ecommerce.commons.dto.ReqProductUpdateDto;
import com.omni.ecommerce.commons.dto.ResProductDto;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.util.UUID;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
class ProductApiTest {

    @Autowired
    private WebApplicationContext wac;

    @Autowired
    private ObjectMapper objectMapper;

    private MockMvc mvc;

    private static final String DETAIL = "Detalle generado desde pruebas de integración";
    private static final String DETAIL_EDIT = "Producto editado";
    private static final String PATH = "/productos";
    private static final String PATH2 = "/productos/";
    private static final int PRICE = 974148;
    private static final int PRICE_UPDATE = 3700;

    @Test
    void validateSave() throws Exception {
        ResProductDto response;

        this.mvc = MockMvcBuilders.webAppContextSetup(this.wac).build();
        String nameProduct = UUID.randomUUID().toString();

        Object resultSaveProduct = mvc.perform(
                MockMvcRequestBuilders.post(PATH)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(new ReqProductCreateDto(
                                nameProduct,
                                PRICE,
                                DETAIL))))
                .andExpect(status().isOk())
                .andReturn()
                .getAsyncResult();


        response = objectMapper.convertValue(resultSaveProduct, ResProductDto.class);

        Object resultSearchProduct = mvc.perform(MockMvcRequestBuilders
                .get(PATH2 + response.getProducto().get(0).getId())
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn()
                .getAsyncResult();
        response = objectMapper.convertValue(resultSearchProduct, ResProductDto.class);

        Assertions.assertEquals(nameProduct, response.getProducto().get(0).getNombre());
        Assertions.assertEquals(PRICE, response.getProducto().get(0).getPrecio());
        Assertions.assertEquals(DETAIL, response.getProducto().get(0).getDescripcion());

    }

    @Test
    void validateUpdate() throws Exception {
        ResProductDto response;

        this.mvc = MockMvcBuilders.webAppContextSetup(this.wac).build();
        String nameProduct = UUID.randomUUID().toString();

        Object resultSaveProduct = mvc.perform(
                MockMvcRequestBuilders.post(PATH)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(new ReqProductCreateDto(
                                nameProduct,
                                PRICE,
                                DETAIL))))
                .andExpect(status().isOk())
                .andReturn()
                .getAsyncResult();


        response = objectMapper.convertValue(resultSaveProduct, ResProductDto.class);

        Object resultUpdateProduct = mvc.perform(
                MockMvcRequestBuilders.put(PATH)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(new ReqProductUpdateDto(
                                response.getProducto().get(0).getId(),
                                nameProduct,
                                PRICE_UPDATE,
                                DETAIL_EDIT))))
                .andExpect(status().isOk())
                .andReturn()
                .getAsyncResult();

        response = objectMapper.convertValue(resultUpdateProduct, ResProductDto.class);

        Assertions.assertEquals(nameProduct, response.getProducto().get(0).getNombre());
        Assertions.assertEquals(PRICE_UPDATE, response.getProducto().get(0).getPrecio());
        Assertions.assertEquals(DETAIL_EDIT, response.getProducto().get(0).getDescripcion());

    }

    @Test
    void validateDelete() throws Exception {
        ResProductDto response;

        this.mvc = MockMvcBuilders.webAppContextSetup(this.wac).build();
        String nameProduct = UUID.randomUUID().toString();

        Object resultSaveProduct = mvc.perform(
                MockMvcRequestBuilders.post(PATH)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(new ReqProductCreateDto(
                                nameProduct,
                                PRICE,
                                DETAIL))))
                .andExpect(status().isOk())
                .andReturn()
                .getAsyncResult();


        response = objectMapper.convertValue(resultSaveProduct, ResProductDto.class);

        Object resultSearchProduct = mvc.perform(MockMvcRequestBuilders
                .delete(PATH2 + response.getProducto().get(0).getId())
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn()
                .getAsyncResult();
        response = objectMapper.convertValue(resultSearchProduct, ResProductDto.class);

        Assertions.assertNotNull(response.getProducto().get(0).getEliminadoEn());
    }

    @Test
    void validateList() throws Exception {
        this.mvc = MockMvcBuilders.webAppContextSetup(this.wac).build();

        Object resultSearchProduct = mvc.perform(MockMvcRequestBuilders
                .get(PATH)
                .param("pagina", "0")
                .param("tamaño", "4")
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn()
                .getAsyncResult();
        ResProductDto response = objectMapper.convertValue(resultSearchProduct, ResProductDto.class);

        Assertions.assertNotNull(response);
        Assertions.assertEquals(response.getResultado().get(0).getDescripcion(), ResponseEnum.SUCCESS.getMessage());
        Assertions.assertEquals(response.getResultado().get(0).getCodigo(), ResponseEnum.SUCCESS.getCode());
        Assertions.assertEquals(response.getResultado().get(0).getTipo(), ResponseEnum.SUCCESS.getType());
        Assertions.assertTrue(response.getProducto().size() > 1);
    }
}